# Contao-Twig-Heroicons

## Function & usage

This extension for Contao Open Source CMS provides a twig function to integrate icons from [Heroicons](https://heroicons.com/) (version `2.1.1`).
It´s inspired by [marcw/twig-heroicons](https://github.com/marcw/twig-heroicons) and customized for use with Contao.

```twig
{# function signature #}
{{ icon(icon, class, style) }}

{# the default style is 'solid' #}
{{ icon('academic-cap') }}

{# use the 'mini' style #}
{{ icon('academic-cap', {}, 'mini') }}

{# Add custom attributes to the SVG #}
{{ icon('academic-cap', { 'class': 'foo bar' }, 'mini') }}
```
