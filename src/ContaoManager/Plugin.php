<?php

namespace designerei\ContaoTwigHeroiconsBundle\ContaoManager;

use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\CoreBundle\ContaoCoreBundle;
use designerei\ContaoTwigHeroiconsBundle\ContaoTwigHeroiconsBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(ContaoTwigHeroiconsBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}