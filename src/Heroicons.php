<?php

namespace designerei\ContaoTwigHeroiconsBundle;

final class Heroicons
{
    const STYLE_SOLID = 'solid';
    const STYLE_OUTLINE = 'outline';
    const STYLE_MINI = 'mini';
    const STYLE_MICRO = 'micro';

    public static function get($icon, $style = self::STYLE_SOLID)
    {
        if (
            self::STYLE_SOLID !== $style &&
            self::STYLE_OUTLINE !== $style &&
            self::STYLE_MINI !== $style &&
            self::STYLE_MICRO !== $style )
        {
            throw new \LogicException(sprintf('Heroicons style "%s" is not available'), $style, $icon);
        }

        $path = sprintf('%s/%s/%s.svg', realpath(__DIR__ . '/../resources'), $style, $icon);
        if (!is_readable($path))
        {
            throw new \LogicException(sprintf('Heroicons "%s" in style "%s" cannot be found or is not readable.', $icon, $style));
        }

        return file_get_contents($path);
    }
}