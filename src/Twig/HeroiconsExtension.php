<?php

namespace designerei\ContaoTwigHeroiconsBundle\Twig;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use Contao\CoreBundle\String\HtmlAttributes;
use designerei\ContaoTwigHeroiconsBundle\Heroicons;

class HeroiconsExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('icon', [$this, 'getHeroicon'], ['is_safe' => ['html']]),
        ];
    }

    public function getHeroicon(string $icon, array|string $attributes = null, string $style = Heroicons::STYLE_OUTLINE)
    {
        $svg = Heroicons::get($icon, $style);

        if (!$attributes) {
            return $svg;
        }

        // check for HtmlAttributes function, introduced in contao 5.0
        if (class_exists('Contao\CoreBundle\String\HtmlAttributes')) {
            $attributes = (new HtmlAttributes($attributes));
        }
        else if (is_array($attributes))
        {
            foreach ($attributes as $attr => $value)
            {
                if ($value)
                {
                    $return[] = ' ' . $attr . '="' . $value . '"';
                }

                else {
                    $return[] = ' ' . $attr;
                }
            }

            $attributes = implode('', $return);
        }

        return str_replace('<svg', sprintf('<svg%s', $attributes), $svg);
    }
}
