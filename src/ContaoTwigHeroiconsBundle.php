<?php

namespace designerei\ContaoTwigHeroiconsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoTwigHeroiconsBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}